function addCell(tr, val) {
    var td = document.createElement('td');
    if (val == undefined){
        var val = "";
    }
    td.innerHTML = val;

    tr.appendChild(td)
}
function addButton(tr, butt_arr) {
    for (var i = 0; i < butt_arr.length; i++) {
        tr.appendChild(butt_arr[i])
    }
}

function addRow(tbl, val_arr,butt_arr) {
    var tr = document.createElement('tr');
    addButton(tr,butt_arr);
    for (var i = 0; i < val_arr.length; i++) {
        addCell(tr,val_arr[i]);
    }

    tbl.appendChild(tr)
}
function haeEventit(startdate,enddate) {

    //var url = "https://api.hel.fi/linkedevents/v1/event/?start=" + startdate+"&format=json&end="+enddate;
    var url = "https://api.hel.fi/linkedevents/v1/event/?end="+enddate+"&format=json&start="+startdate;
    //console.log(url)
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var arr = JSON.parse(xmlhttp.responseText);
            //  console.log("Tuloksia: "+arr.meta.count+" kpl")
            var events = arr.data;
            var table = document.getElementById("tableDaily");

            addRow(table,["Arvosana", "Nimi", "Alkaa", "Loppuu", "Lyhyt kuvaus"],[] )

            for (var i = 0; i < arr.data.length; i++) {

                var event = events[i];
                var napit = [];
                if(event.name.fi == null){
                    continue;
                }
                for(var o = 0;o<=5;o++){
                    var nappi = document.createElement("button");
                    nappi.id = i+""+o;
                    var obj = {value: o, name: event.name.fi, start:event.start_time, end:event.end_time, desc:event.short_description.fi,id:event.id }
                    nappi.value = JSON.stringify(obj);
                    nappi.innerHTML = o;
                    napit.push(nappi);
                    nappi.onclick =  function () {
                        var xmlhttp1 = new XMLHttpRequest();
                        var urli = "http://localhost:8082/api/rate?id="+this.id+"&value="+this.value;
                        //  console.log(url);
                        xmlhttp1.onreadystatechange = function() {
                            if (xmlhttp1.readyState === 4 && xmlhttp1.status === 200) {
                                console.log(xmlhttp1.responseText)
                            }};
                        xmlhttp1.open("GET",urli , true);
                        // xmlhttp1.setRequestHeader("Content-Type", "application/json");
                        xmlhttp1.send();
                        for(var k = 0;k<=5;k++){
                            document.getElementById(this.id.slice(0,-1) + ""+k).style.display = "none";
                            if(k == 5){
                                var el =document.getElementById(this.id.slice(0,-1) + ""+k)

                                var newEl = document.createElement('p');
                                newEl.innerHTML = '<b>Kiitos arvostelusta!</b>';
                                el.parentNode.replaceChild(newEl, el);
                            }
                        }

                    }
                }


                try {
                    addRow(table, [event.name.fi, event.start_time, event.end_time,event.short_description.fi],napit);
                }
                catch(err) {
                    console.log("Erroria datan haussa")
                    continue;
                }

            }

        }


    }

    //https://api.hel.fi/linkedevents/v1/event/?end=2014-01-20&format=json&start=2014-01-15
    xmlhttp.open("GET",url, true);
    xmlhttp.send();

}
function updateTopBest() {
    var url = "http://localhost:8082/api/top5-best/alltime";
    //console.log(url)
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var arr = JSON.parse(xmlhttp.responseText);

            var events = arr;
            var table = document.getElementById("tableTopBest");

            addRow(table, ["Nimi", "Alkaa", "Arvosana"], [])
            for (var i = 0; i < arr.length; i++) {
                var event = events[i];
                try {
                    addRow(table, [event.name, event.start_date,event.rating], []);
                }
                catch (err) {
                    console.log("Erroria datan haussa")
                    continue;
                }
            }
        }
    }
    xmlhttp.open("GET",url, true);
    xmlhttp.send();

}
function updateTopWorst() {
    var url = "http://localhost:8082/api/top5-worst/alltime";
    //console.log(url)
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var arr = JSON.parse(xmlhttp.responseText);

            var events = arr;
            var table = document.getElementById("tableTopWorst");

            addRow(table, ["Nimi", "Alkaa", "Arvosana"], [])
            for (var i = 0; i < arr.length; i++) {
                var event = events[i];
                try {
                    addRow(table, [event.name, event.start_date,event.rating], []);
                }
                catch (err) {
                    console.log("Erroria datan haussa")
                    continue;
                }
            }
        }
    }
    xmlhttp.open("GET",url, true);
    xmlhttp.send();

}

function updateSearchTable() {
    var name = document.getElementById("search").value;
    var table = document.getElementById("tableSearch");
    while(table.firstChild){
        table.removeChild(table.firstChild);
    }
    var url = "http://localhost:8082/api/name/?name="+name;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var events = JSON.parse(xmlhttp.responseText);
            if(events.isEmpty || events.length === 0){
                addRow(table, ["Hakusanalla "+name+" ei löytynyt yhtään tapahtumaa"],[]);
            }else {
                addRow(table,["Nimi", "Alkaa", "Arvosana"],[] )
                for (var i = 0; i < events.length; i++) {
                    var event = events[i];
                    addRow(table, [event.name, event.start_date,event.rating],[]);
                }
            }
        }
    };
    xmlhttp.open("GET",url, true);
    xmlhttp.send();

}

function vaihdaPaiva() {
    var table = document.getElementById("tableDaily");
    while(table.firstChild){
        table.removeChild(table.firstChild);
    }
    var paivaHaku = document.getElementById("paivaHaku");
    var paiva = paivaHaku.value;
    console.log(paiva);
    haeEventit(paiva,paiva);
}

updateTopWorst();
updateTopBest();
haeEventit("today","today");