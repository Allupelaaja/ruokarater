var mysql = require('mysql');
var express = require('express');
var bodyParser = require('body-parser');
const request = require('request');
var moment = require("moment");
const utf8 = require('utf8');
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname ));
var port = 8082;

var server = app.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://localhost:8082")
});
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "ruokaraterdb"
});

app.get('/',function(req,res) {
    res.sendFile("index.html", { root: __dirname });
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get("/api/rate",function(req,res){
    var valueJson = JSON.parse(req.query.value);
    var rateValue = valueJson.value;
    var name = valueJson.name;
    var start_date = valueJson.start;
    var end_date = valueJson.end;
    var eventId = valueJson.id;
    if(start_date == null){
        start_date = end_date;
        if(start_date == null){
            start_date = "1000-01-01";
        }
    }
    if(end_date == null){
        end_date =start_date;
        if(end_date == null){
            end_date ="3000-01-01";
        }
    }
    start_date = moment(start_date).format('YYYY-MM-DD HH:mm:ss');
    end_date = moment(end_date).format('YYYY-MM-DD HH:mm:ss');

    var short_description = valueJson.desc;
    if(null != short_description && undefined != short_description){
        utf8.encode(short_description)
    }

    var sameEventId_id = -1;
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id";
    con.query(sql, function (err, result) {
        if (err) throw err;
        for (var i = 0; i < result.length; i++) {
            if(result[i].event_id === eventId){
                sameEventId_id = result[i].Rates_id;
            }
        }
        if(sameEventId_id >= 0){
            sql = "UPDATE rates SET rate_count = rate_count + 1 , " +
                "star_count = star_count + ? , rating = star_count / rate_count" +
                " WHERE Rates_id = ?";
            con.query(sql,[rateValue,sameEventId_id], function (err, result) {
                //console.log("päivitettiin rates taulukkoa")
                if (err) throw err;

            });
            console.log("Päivitettiin olemassaolevaa riviä");
        } else {
            var insertId = 0;
            sql = "INSERT INTO events (event_id, name, start_date, end_date, short_description) VALUES (?,?,?,?,?) ";
            con.query(sql,[eventId,name,start_date,end_date,short_description], function (err, result) {
              //  console.log("Luotiin uusi event")
                if (err) throw err;
                insertId = result.insertId;

            sql = "INSERT INTO rates (event_id_id,rate_count, star_count, rating) VALUES (?,?,?,?) ";
            con.query(sql,[insertId,1,rateValue,rateValue], function (err, result) {
               // console.log("Luotiin uusi rate")
                if (err) throw err;
            })
            });
            console.log("Luotiin uusi rivi");

        }
    });

console.log("kaikki okei");
res.send("Kaikki okei");
});
app.get('/api/top5-best/alltime',function(req,res) {

    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id ORDER BY rating DESC LIMIT 5";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});
app.get('/api/top5-worst/alltime',function(req,res) {
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id ORDER BY rating ASC LIMIT 5";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});
app.get('/api/top5-worst/',function(req,res) {
    var start = req.query.start;
    var end = req.query.end;
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id and " +
        "start_date >= ? and start_date <= ? ORDER BY rating ASC LIMIT 5";
    con.query(sql,[start,end], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});
app.get('/api/top5-best/',function(req,res) {
    var start = req.query.start;
    var end = req.query.end;
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id and " +
        "start_date >= ? and start_date <= ? ORDER BY rating DESC LIMIT 5";
    con.query(sql,[start,end], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/everything/alltime',function(req,res) {

    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id ORDER BY name ";
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/everything/',function(req,res) {
    var start = req.query.start;
    var end = req.query.end;
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id and " +
        "start_date >= ? and start_date <= ? ORDER BY name ";
    con.query(sql,[start,end], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});

app.get('/api/name/',function(req,res) {
    var name = req.query.name;
    var sql = "SELECT * FROM rates,events WHERE event_id_id = Events_id and " +
        "name = ? ORDER BY start_date";
    con.query(sql,[name], function (err, result) {
        if (err) throw err;
        res.send(result);
    });
});
